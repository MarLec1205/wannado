import React from 'react'
export default class Useres extends React.Component{
    constructor(props){
        super(props);
        this.state={
            useres: this.props.useres,
        };
    }
    render(){
        const user= this.state.useres.map( el =>
            (<div className='userElement'>
                <h3>{el.userName}</h3>
                <div className='userDetails'>
                    <div className='details'>
                        <div className='name'> {el.name}</div>
                        <div className='age'>{el.age} lat</div>
                        <div className='userWannaDo'> WannaDO! :{el.wannaDo.map(act=>  <span> {act}, </span>)}</div>
                    </div>
                    <div className='about'>
                        {el.about}
                    </div>
                </div>

            </div>)
        );

        return(
            <div className='useres'>{user}</div>
        );
    }
}