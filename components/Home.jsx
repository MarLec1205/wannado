import React from 'react';
import {Link} from 'react-router-dom';

export default class Home extends React.Component{
    constructor(props){
        super(props);
        this.state={
            msgVisibilities: "none",
            msgPpl: [],
            msgContent: [],
            msgArea:''

        };
    }
    changeHandle =(e) =>{
        this.setState({
            [e.target.name]: e.target.value
        })

    };
    delete = (e) =>{
        fetch('http://localhost:3000' + '/activities/' + e.target.name, {
            method: 'delete'
        }).then(response =>
            response.json().then(json => json)
        )
    };

    btnMsg = () =>{
        let visibility ='';
        this.state.msgVisibilities==='none'? visibility="block" : visibility="none";
        this.setState({
            msgVisibilities: visibility

        })
    };
    componentDidMount(){
        fetch(`http://localhost:3000/messages`).
        then(resp =>{
            if (resp.ok)
                return resp.json();
            else
                throw new Error('Błąd sieci!');
        } ).then( data => {
            this.setState({
                msgPpl: data,
            });
        });
        fetch(`http://localhost:3000/message`).
        then(resp =>{
            if (resp.ok)
                return resp.json();
            else
                throw new Error('Błąd sieci!');
        } ).then( data => {
            this.setState({
                msgContent: data,
            });
        });

    }
    send = (id ) => {

        let currentdate = new Date();
        let date =currentdate.getFullYear() + "-"+(currentdate.getMonth()+1) + "-" +(currentdate.getDate());
        let time =currentdate.getHours()+":"+currentdate.getMinutes();
        let obj ={};
        obj.messageId = Number(id);
        obj.name= this.props.loggedUser;
        obj.text = this.state.mssg;
        obj.date = date;
        obj.time = time;
        console.log(obj.messageId);
        fetch('http://localhost:3000' + "/messages/"+obj.messageId+"/message", {
            method: 'POST',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json',
            },
            body: JSON.stringify(obj )
        }).then( resp => resp.json()).then( data => {
            console.log( data );
        });
    };
    render(){
        console.log(this.state.msgPpl, "msgppl");
        console.log(this.state.msgContent, "msgcon");
        let myMsg = this.state.msgPpl.map(el =>{
                if(el.user1 === this.props.loggedUser || el.user2 === this.props.loggedUser){


                    let oneMsg = this.state.msgContent.map(msg => {
                        if(el.id === Number(msg.messageId)){
                            return(
                                <div>
                                    <span> {msg.date} {msg.time}</span> <br/>
                                    <span> {msg.name}</span> <br/>
                                    <span> {msg.text}</span>
                                </div>
                            )
                        }
                    });
                    let disp="none";
                    if(el.user1 === this.props.loggedUser){
                        return(<div>
                                <li > {el.user2}</li>
                                <div> {oneMsg}</div>
                                <input className='msgArea' type="text" onChange={this.changeHandle} name='mssg'/>
                                <button onClick={(e) =>this.send(el.id)}><Link to="/">Wyślij</Link></button>
                            </div>
                        )

                    }else{
                        return(<div>
                                <li > {el.user1}</li>
                                <div> {oneMsg}</div>
                                <input className='msgArea' type="text" onChange={this.changeHandle} name='mssg'/>
                                <button onClick={(e) =>this.send(el.id)}>Wyślij</button>
                            </div>
                        )
                    }
                }
            }
        );


        let useres = this.props.useres.slice();
        let activities = this.props.activities.slice();
        let activitiesArr = [];
        let user = {};
        for(let el of useres) {

            if (el.userName === this.props.loggedUser) {
                user.userName = el.userName;
                user.pswd = el.pswd;
                user.name = el.name;
                user.age = el.age;
                user.about = el.about;
                user.wannaDo = el.wannaDo;
            }
        }
        for(let el of activities){
            if(el.user === this.props.loggedUser){
                let obj={};
                obj.user= el.user;
                obj.sport= el.sport;
                obj.place= el.place;
                obj.date= el.date;
                obj.district=el.district;
                obj.time= el.time;
                obj.lvl= el.lvl;
                obj.desc=el.desc;
                activitiesArr.push(obj);
            }
        }
        const list = activitiesArr.map( el =>
            <div className='sportWannaDo'>
                <p className='wannaDO'> {el.user} WannaDO! : {el.sport}</p>
                <p className='place'>{el.place} - {(el.district==='')? "całe miasto": el.district}</p>
                <p> LVL :  {el.lvl}</p>
                <p>{el.date} godz {el.time}</p>
                <p className='desc'> "{el.desc}"</p>
                <button name={el.id} onClick={this.delete}> USUŃ </button>
            </div>
        );

        if(this.props.loggedUser === ''){
            return(
                <div className="home">
                    Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.

                </div>
            );
        } else {
            return(
                <div className='logedUser'>
                    <button onClick={this.btnMsg}> WIADOMOŚCI </button>
                    <ul style={{display: this.state.msgVisibilities}}>
                        {myMsg}
                    </ul>
                    <div className='userElement'>
                        <h3>{user.userName}</h3>
                        <div className='userDetails'>
                            <div className='details'>
                                <div className='name'> {user.name}</div>
                                <div className='age'>{user.age} lat</div>
                                <div className='userWannaDo'> WannaDO! :{user.wannaDo.map(act=>  <span> {act}, </span>)}</div>
                            </div>
                            <div className='about'>
                                {user.about}
                            </div>
                        </div>

                    </div>
                    <div className='activities'>
                        {list}
                    </div>
                </div>
            )
        }
    }
}