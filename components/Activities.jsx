import React from 'react'
import AddWannaDo from './AddWannaDo.jsx';

export default class Activities extends React.Component{
    constructor(props){
        super(props);
        this.state={
            message: [],
            msgDisplay: "none",
            lokation: '',
            activity: '',
            date: '',
            // wannado: this.props.activities,
            addActivity:'',
            addLokation: '',
            addUserName:'',
            addDate: '',
            addTime: '',
            addLvl: '',
            addDesc: '',
            dist: '',
            addVisibility: 'none',
            searchVisibility: 'none',
            mssg: '',
        };
    }
    addVisibility = () => {
        let display = '';
        (this.state.addVisibility==='none')? display='flex' : display='none';
        this.setState({
            addVisibility: display,
        })
    };
    searchVisibility = () => {
        let display = '';
        (this.state.searchVisibility==='none')? display='flex' : display='none';
        this.setState({
            searchVisibility: display,
        })
    };
    addWannaDO= () => {
        //let arr = this.state.wannado.slice();
        let obj={};
        obj.user= this.props.loggedUser;
        obj.sport= this.state.addActivity;
        obj.place= this.state.addLokation;
        obj.date= this.state.addDate;
        obj.district=this.state.dist;
        obj.time= this.state.addTime;
        obj.lvl= this.state.addLvl;
        obj.desc=this.state.addDesc;
        //arr.push(obj);

        fetch('http://localhost:3000' + "/activities/", {
            method: 'POST',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json',
            },
            body: JSON.stringify({
                id: obj.user+Math.random(),
                user: obj.user,
                sport: obj.sport,
                place: obj.place,
                date: obj.date,
                district: obj.district,
                time: obj.time,
                lvl: obj.lvl,
                desc: obj.desc
            } )
        }).then( resp => resp.json()).then( data => {
            console.log( data );
        })
    };
    changeHandle = (e) => {
        this.setState({
            [e.target.name]: e.target.value,
        })
    };
    handle = (name, value) => {
        this.setState({
            [name]: value,
        })
    };
    delete = (e) =>{
        fetch('http://localhost:3000' + '/activities/' + e.target.name, {
            method: 'delete'
        }).then(response =>
            response.json().then(json => json)
        )
    };
    send = (user) => {
        console.log("send");
        let msg = this.props.messages;
        let test = true;
        let currentdate = new Date();
        let date =currentdate.getFullYear() + "-"+(currentdate.getMonth()+1) + "-" +(currentdate.getDate());
        let time =currentdate.getHours()+":"+currentdate.getMinutes();
        let obj={};

        msg.map(el => {
            if(el.user1===user && el.user2===this.props.loggedUser || el.user1===this.props.loggedUser && el.user2===user ){
                let mess = fetch(`http://localhost:3000/messages/`+el.id+"/message").
                then(resp =>{
                    if (resp.ok)
                        return resp.json();
                    else
                        throw new Error('Błąd sieci!');
                } ).then( data => {
                    this.setState({
                        message: data,
                    });
                });
                obj.messageId = el.id;
                obj.name= this.props.loggedUser;
                obj.text = this.state.mssg;
                obj.date = date;
                obj.time = time;
                console.log(obj);
                fetch('http://localhost:3000' + "/messages/"+el.id+"/message", {
                    method: 'POST',
                    headers: {
                        'Accept': 'application/json',
                        'Content-Type': 'application/json',
                    },
                    body: JSON.stringify(obj )
                }).then( resp => resp.json()).then( data => {
                    console.log( data );
                });
                test=false;
            }
        });
        if(test){
            let usrs = {};
            usrs.id =this.props.messages.length+1;
            usrs.user1=user;
            usrs.user2=this.props.loggedUser;

            obj.messageId = usrs.id;
            obj.id= 1;
            obj.name= this.props.loggedUser;
            obj.text = this.state.mssg;
            obj.date = date;
            obj.time = time;

            fetch('http://localhost:3000' + "/messages/", {
                method: 'POST',
                headers: {
                    'Accept': 'application/json',
                    'Content-Type': 'application/json',
                },
                body: JSON.stringify({
                    id: usrs.id,
                    user1 : usrs.user1,
                    user2 : usrs.user2
                })
            }).then( resp => resp.json()).then( data => {
                console.log( data );
            }).catch(er => console.log(er));
            fetch('http://localhost:3000' + "/message", {
                method: 'POST',
                headers: {
                    'Accept': 'application/json',
                    'Content-Type': 'application/json',
                },
                body: JSON.stringify({
                    messageId : obj.messageId,
                    name: obj.name,
                    text : obj.text,
                    date : obj.date,
                    time : obj.time
                } )
            }).then( resp => resp.json()).then( data => {
                console.log( data );
            });

        }
        this.setState({
            msgDisplay: 'none'
        })
    };
    msgClick =() => {
        let cash = '';
        this.state.msgDisplay === "none" ? cash="block" : cash= 'none';
        this.setState({
            msgDisplay: cash,
        })
    };
    render(){
        let arr= [];
        this.props.activities.map(el =>
            (el.place.toLowerCase().includes(this.state.lokation.toLowerCase()) && (el.sport.toLowerCase().includes(this.state.activity.toLowerCase())) &&(el.date.toLowerCase().includes(this.state.date.toLowerCase())) ) ? arr.push(el) : {}
        );
        const list = arr.map( el => {
                if(this.props.loggedUser === ''){
                    return(
                        <div className='sportWannaDo'>
                            <p className='wannaDO'> {el.user} WannaDO! : {el.sport}</p>
                            <p className='place'>{el.place} - {(el.district === '') ? "całe miasto" : el.district}</p>
                            <p> LVL : {el.lvl}</p>
                            <p>{el.date} godz {el.time}</p>
                            <p className='desc'> "{el.desc}"</p>

                        </div>
                    );
                } else {
                    if (el.user === this.props.loggedUser) {
                        return (
                            <div className='sportWannaDo'>
                                <p className='wannaDO'> {el.user} WannaDO! : {el.sport}</p>
                                <p className='place'>{el.place} - {(el.district === '') ? "całe miasto" : el.district}</p>
                                <p> LVL : {el.lvl}</p>
                                <p>{el.date} godz {el.time}</p>
                                <p className='desc'> "{el.desc}"</p>
                                <button name={el.id} onClick={this.delete}> USUŃ</button>

                            </div>
                        );
                    } else {
                        return (
                            <div className='sportWannaDo'>
                                <p className='wannaDO'> {el.user} WannaDO! : {el.sport}</p>
                                <p className='place'>{el.place} - {(el.district === '') ? "całe miasto" : el.district}</p>
                                <p> LVL : {el.lvl}</p>
                                <p>{el.date} godz {el.time}</p>
                                <p className='desc'> "{el.desc}"</p>
                                <button  onClick={this.msgClick}> Napisz Wiadomość</button>
                                <div style={{display: this.state.msgDisplay}}>
                                    <input className='msgArea' type="text" onChange={this.changeHandle} name='mssg'/>
                                    <button onClick={(e) =>this.send(el.user)}>Wyślij</button>
                                </div>
                            </div>
                        );
                    }
                }
            }
        );
        return(
            <div className="activities">
                <button onClick={this.searchVisibility}>Szukaj</button>
                <button onClick={this.addVisibility}> Dodaj</button>
                <div className='search' style={{display: this.state.searchVisibility}} >
                    <p> Wpisz nazwe miasta: <br/>
                        <input name='lokation' onChange={this.changeHandle} type="text" value={this.state.lokation} placeholder='Miasto'/>
                    </p>
                    <p > Wpisz nazwe sportu: <br/>
                        <input name='activity' onChange={this.changeHandle} type="text" value={this.state.activity} placeholder='Wpisz co WannnaDo!'/>
                    </p>
                    <p > Wbierz date <br/>
                        <input name= 'date' onChange={this.changeHandle} type="date" value={this.state.date} placeholder='Data w formacie DD.MM'/>
                    </p>
                </div>
                <div style={{display: this.state.addVisibility}}>
                    <AddWannaDo handle={this.handle} addWannaDO={this.addWannaDO}/>
                </div>
                {list}
            </div>
        );
    }
}