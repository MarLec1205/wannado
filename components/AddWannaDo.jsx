import React from "react";
export default class AddWannaDo extends React.Component{

    changeHandle = (e) => {

        this.props.handle(e.target.name, e.target.value);
    };
    wannaDo = () =>{
        this.props.addWannaDO();
    };
    render(){
        return(
            <form className='addWannaDoForm' type="submit" onSubmit={this.wannaDo}>
                <h3>Nie znalazłeś nic dla siebie? Dodaj własne WannaDo!</h3>
                <div className='addWannaDoDiv'>
                    <p>Nazwa Sportu: <br/>
                        <input name='addActivity' type="text" onChange={this.changeHandle} />
                    </p>
                    <p>Nazwa Miasta: <br/>
                        <input name='addLokation' type="text" onChange={this.changeHandle} />
                    </p>
                    <p>Nazwa dzielnicy:<br/>
                        <input name='dist' type="text" onChange={this.changeHandle}/>
                    </p>
                    <p>Data: <br/>
                        <input name='addDate' type="date" onChange={this.changeHandle} />
                    </p>
                    <p>Godzina: <br/>
                        <input name='addTime' type="time" onChange={this.changeHandle}/>
                    </p>
                    <p>Poziom zawansowania<br/>
                        <select name="addLvl" onChange={this.changeHandle}>
                            <option value="początkujący">początkujący</option>
                            <option value="początkujący +">początkujący +</option>
                            <option value="średnio-zaawansowany">średnio-zaawansowany</option>
                            <option value="zaawansowany">zaawansowany</option>
                            <option value="zaawansowany +">zaawansowany+</option>
                            <option value="ekspert">ekspert</option>
                        </select>


                    </p>
                    <p>Zachęć innych ;) <br/> <textarea onChange={this.changeHandle} name="addDesc" id="" cols="30" rows="10" placeholder="Why u should WannaDO with me ;)"/></p>
                </div>
                <button type='submit' >WannaDO!</button>
            </form>
        );
    }
}
