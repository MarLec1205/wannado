import React from 'react';

export default class Login  extends React.Component{


    changeHandle = (e) => {

        this.props.handle(e.target.name, e.target.value);
    };
    login =(e) =>{
        e.preventDefault();
        this.props.login();

    };
    render(){
        return(
            <div>
                <form onSubmit={this.login} type="submit">
                <p> Twoja nazwa użytkownika:
                    <input onChange={this.changeHandle} type="text" name='loginUserName'/>
                </p>
                <p> Twoje hasło:
                    <input  onChange={this.changeHandle} type='password' name="loginPassword"/>
                </p>
                    <button type='submit'>  ZALOGUJ</button>
                </form>
            </div>

        );
    }
}