import React from 'react';
import Navigation from '../components/Navigation.jsx';
import {
    HashRouter,
    Route,
    Link,
    Switch,
    NavLink,
} from 'react-router-dom';

export default class Header extends React.Component{

    logingOut = (e) => {
        this.props.logout();
        window.location = '#/login'
    };
    render(){

        return(
            <div className="header">
                <h1>WannaDO!</h1>
                <h3>Let's train together</h3>
                <button style={{display: (this.props.loggedUser==='') ? "block": "none" }}><Link to='login'>Zaloguj się </Link></button>
                <button style={{display: (this.props.loggedUser==='') ? "block": "none"}}><Link to='register'>Zarejestuj się </Link></button>
                <button style={{display: (this.props.loggedUser==='') ? "none" : "block"}}  onClick={this.logingOut} >Wyloguj się</button>
                <Navigation/>
            </div>
        );
    }
}