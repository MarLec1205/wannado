import React from 'react';
import {
    HashRouter,
    Route,
    Link,
    Switch,
    NavLink,
} from 'react-router-dom';

export default class Register extends React.Component{

    changeHandle = (e) => {

        this.props.handle(e.target.name, e.target.value);
    };
    register = (e) => {
        this.props.register();
        window.location = '#/login/'
    };
    render(){
        return(
            <form type="submit" onSubmit={this.register}>
                <p> Nazwa użytkownika <br/>
                    <input onChange={this.changeHandle} name="userName" type="text" placeholder='UserName'/>
                </p>
                <p> Hasło <br/>
                    <input onChange={this.changeHandle} name='pswd' type="password" placeholder='twoje hasło'/>
                </p>
                <p> Imię <br/>
                    <input onChange={this.changeHandle} name='name' type="text" placeholder='Twoje imię'/>
                </p>
                <p> Wiek <br/>
                    <input onChange={this.changeHandle} name='age' type="number" placeholder='Ile masz lat?'/>
                </p>
                <p> Napisz coś o sobie <br/>
                    <textarea onChange={this.changeHandle} name='about' type="text" placeholder="pare zdań żeby inny wiedzieli kim jestesś" cols="30" rows="10" />
                </p>
                <p> Jakie sporty chętnie uprawiasz <br/>
                    <input onChange={this.changeHandle} name='wannaDo' type="text" placeholder="jakie sporty uprawiasz?" />
                </p>
                <button type='submit'>Zarejestruj</button>
            </form>
        );
    }
}