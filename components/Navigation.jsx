import React from 'react';
import {
    HashRouter,
    Route,
    Link,
    Switch,
    NavLink,
} from 'react-router-dom';

export default class Navigation extends React.Component{
    constructor(props){
        super(props);
        this.state={
        display: 'none',
        };
    }
    btnClick =() => {
        let disp = '';
        (this.state.display === 'none') ? disp ='flex' : disp='none';
        this.setState({
            display: disp,
        })
    };
    render(){
        return(<div>
            <button onClick={this.btnClick} className='naviButton'> MENU</button>
            <ul style={{display: this.state.display}} className='navList'>
                <li><Link onClick={this.btnClick}  to="/"> HOME </Link></li>
                <li><Link onClick={this.btnClick} to="activities">sth WannaDO!</Link></li>
                <li><Link onClick={this.btnClick} to='useres'>who WannaDO! sth</Link></li>
                <li>Who are we</li>
            </ul>
            </div>
        );
    }
}
