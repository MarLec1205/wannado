import React from 'react';
import ReactDOM from 'react-dom';
import {
    HashRouter,
    Route,
    Link,
    Switch,
    NavLink,
} from 'react-router-dom';

import Header from '../components/Header.jsx';
import Home from '../components/Home.jsx';
import Activities from '../components/Activities.jsx';
import Useres from '../components/Useres.jsx';
import Register from '../components/Register.jsx';
import Login from '../components/Login.jsx';


import './../sass/style.scss'; // adres do głównego pliku SASS

class App extends React.Component {
    constructor(props){
        super(props);

        this.state={
            id: '',
            useres: [],
            activities: [],
            userName: '',
            pswd: '',
            name: '',
            age: '',
            about: '',
            wannaDo: [],
            loginUserName: '',
            loginPassword: '',
            loggedUser:'',
            messages: {},
        };
    }
    componentDidMount() {
        fetch(`http://localhost:3000/activities`).
        then(resp =>{
            if (resp.ok)
                return resp.json();
            else
                throw new Error('Błąd sieci!');
        } ).then( data => {
            console.log(data);
            this.setState({
                activities: data,
            });
        });

        fetch(`http://localhost:3000/messages`).
        then(resp =>{
            if (resp.ok)
                return resp.json();
            else
                throw new Error('Błąd sieci!');
        } ).then( data => {
            this.setState({
                messages: data,
            });
        });
        fetch(`http://localhost:3000/useres`).
        then(resp =>{
            if (resp.ok)
                return resp.json();
            else
                throw new Error('Błąd sieci!');
        } ).then( data => {
            this.setState({
                useres: data,
            });
        });
    }

    registerButton = () => {
        let obj={};
        let arr = this.state.useres.slice();
        obj.id = this.state.userName;
        obj.userName = this.state.userName;
        obj.pswd= this.state.pswd;
        obj.name = this.state.name;
        obj.age = this.state.age;
        obj.about = this.state.about;
        obj.wannaDo = this.state.wannaDo;
        arr.push(obj);
        fetch('http://localhost:3000' + "/useres/", {
            method: 'POST',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json',
            },
            body: JSON.stringify({
                id: obj.id,
                userName: obj.userName,
                pswd: obj.pswd,
                name: obj.name,
                age: obj.age,
                about: obj.about,
                wannaDo: obj.wannaDo
            } )
        }).then( resp => resp.json()).then( data => {
            console.log( data );
        })

    };



    logout = () =>{
        this.setState({
            loggedUser: '',
        })
    };

    loginButton = () =>{
        let pswd= '';
        let test = false;
        for(let el of this.state.useres){
            (el.userName === this.state.loginUserName) ? pswd = el.pswd : {};
        }
        (pswd === this.state.loginPassword) ? test= true : test=false;
        if(test){
            this.setState({
                loggedUser: this.state.loginUserName,
            });
            window.location = '#/';
        }else{
            alert("zła nazwa użytkownika lub hasło");
        }

    };
    handle = (name, value) => {
        (name === 'wannaDo')? value=value.split(",") : {};
        this.setState({
            [name]: value,
        })
    };

    render() {

        return (
            <HashRouter>
                <div className='container'>
                    {console.log(this.state.messages)}
                    <Header logout={this.logout} loggedUser={this.state.loggedUser} />
                    <Route exact path='/' render={(props) => <Home  useres={this.state.useres} loggedUser={this.state.loggedUser} activities={this.state.activities} {...props} />}/>
                    <Route path='/activities' render={(props) => <Activities messages={this.state.messages} loggedUser={this.state.loggedUser} activities={this.state.activities} {...props} />}/>
                    <Route path='/useres' render={(props) => <Useres useres={this.state.useres} {...props} />}/>
                    <Route path='/register' render={(props) => <Register register={this.registerButton} handle={this.handle} {...props} />}/>
                    <Route path='/login' render={(props) => <Login login={this.loginButton} handle={this.handle} {...props} />}/>
                </div>
            </HashRouter>

        );
    }
}

document.addEventListener('DOMContentLoaded', function() {
    ReactDOM.render(
        <App />,
        document.getElementById('app')
    )
})